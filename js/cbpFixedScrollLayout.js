
var cbpFixedScrollLayout = (function() {

	// cache and initialize some values
	var config = {
		// the cbp-fbscroller's sections
		$sections : $( '#cbp-fbscroller > section' ),
		// the navigation links
		$navlinks : $( '#cbp-fbscroller > nav:first > a' ),
		// index of current link / section
		currentLink : 0,
		// the body element
		$body : $( 'html, body' ),
		// the body animation speed
		animspeed : 650,
		// the body animation easing (jquery easing)
		animeasing : 'easeInOutExpo'
	};

	function init() {

		// click on a navigation link: the body is scrolled to the position of the respective section
		config.$navlinks.on( 'click', function() {
			scrollAnim( config.$sections.eq( $( this ).index() ).offset().top );
			return false;
		} );

		// 2 waypoints defined:
		// First one when we scroll down: the current navigation link gets updated. A "new section" is reached when it occupies more than 70% of the viewport
		// Second one when we scroll up: the current navigation link gets updated. A "new section" is reached when it occupies more than 70% of the viewport
		config.$sections.waypoint( function( direction ) {
			if( direction === 'down' ) { changeNav( $( this ) ); }
		}, { offset: '30%' } ).waypoint( function( direction ) {
			if( direction === 'up' ) { changeNav( $( this ) ); }
		}, { offset: '-30%' } );

		fixed_bg();
	}

	// update the current navigation link
	function changeNav( $section ) {
		config.$navlinks.eq( config.currentLink ).removeClass( 'cbp-fbcurrent' );
		config.currentLink = $section.index( 'section' );
		config.$navlinks.eq( config.currentLink ).addClass( 'cbp-fbcurrent' );
	}

	// function to scroll / animate the body
	function scrollAnim( top ) {
		config.$body.stop().animate( { scrollTop : top }, config.animspeed, config.animeasing );
	}

	// function to set bg fixed and nav absolute
	function fixed_bg() {
		$('#fbsection1').waypoint(function() {
			$(this).toggleClass("hit-fixed");
		}, { offset: '5%' });

		$('#fbsection2').waypoint(function() {
			$(this).toggleClass("hit-fixed");
		}, { offset: '30%' });

		$('#fbsection3').waypoint(function() {
			$(this).toggleClass("hit-fixed");
		}, { offset: '30%' });

		$('#fbsection4').waypoint(function() {
			$(this).toggleClass("hit-fixed");	
		}, { offset: '30%' });

		// hitting the last section, slider nav will be absolute position.
		$('#cbp-fbscroller section:nth-last-child(2)').waypoint(function() {
			$('.slider-nav').toggleClass("nav-absolute");
		}, { offset: '0' });

		// slider nav fades out when scrolling to the slider section
		$('#fbsection1').waypoint(function() {
			$('.slider-nav').toggleClass("nav-show");
		}, { offset: '5%' });
	}

	return { init : init };

})();

